--[[ NekoEngine
--
--   FPSController.lua
--   Author: Alexandru Naiman
--
--   First-Person camera controller
--
--   -----------------------------------------------------------------------------
--
--   Copyright (c) 2015-2016, Alexandru Naiman
--
--   All rights reserved.
--
--   Redistribution and use in source and binary forms, with or without
--   modification, are permitted provided that the following conditions are met:
--
--   1. Redistributions of source code must retain the above copyright notice,
--   this list of conditions and the following disclaimer.
--
--   2. Redistributions in binary form must reproduce the above copyright notice,
--   this list of conditions and the following disclaimer in the documentation
--   and/or other materials provided with the distribution.
--
--   3. Neither the name of the copyright holder nor the names of its contributors
--   may be used to endorse or promote products derived from this software without
--   specific prior written permission.
--
--   THIS SOFTWARE IS PROVIDED BY ALEXANDRU NAIMAN "AS IS" AND ANY EXPRESS OR
--   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARANTIES OF
--   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
--   IN NO EVENT SHALL ALEXANDRU NAIMAN BE LIABLE FOR ANY DIRECT, INDIRECT,
--   INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
--   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
--   OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
--   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
--   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
--   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--]]

local verticalSensivity = 2500.0
local horizontalSensivity = 2500.0
local moveSpeed = 40.0
local sprintSpeed = 60.0
local rotateSpeed = 40.0
local cameraComponent = nil
local camera = nil

function Load()
	cameraComponent = E_NewComponent("CameraComponent", parent, "near=0.1", "far=1000.0", "fov=90", "projection=perspective", "position=-40.0, 30.0, 0.0", "rotation=0.0, 0.0, 0.0", "fog_color=0.0, 0.0, 0.0", "view_distance=1000", "fog_distance=1200")
	OC_Load(cameraComponent)
	O_AddComponent(parent, "FPSCamera", cameraComponent)
	camera = CC_GetCamera(cameraComponent)
	CM_SetActiveCamera(camera)

	if not I_EnableMouseAxis(true) then
		L_Log("FPSController", LOG_WARNING, "Failed to enable mouse axis")
	end

	return 0
end

function Update(deltaTime)
	local vAngle = I_GetAxis("vertical") * verticalSensivity
	local hAngle = I_GetAxis("horizontal") * horizontalSensivity

	local posPtr = O_GetPosition(parent)
	local rotPtr = O_GetRotation(parent)
	local pos = ffi.cast("vec3 *", posPtr)
	local rot = ffi.cast("vec3 *", rotPtr)

	rot.x = rot.x + math.rad(vAngle)
	rot.y = rot.y + math.rad(hAngle)

	local speed = moveSpeed

	if I_GetButton("sprint") == true then
		speed = sprintSpeed
	end

	local velocity = deltaTime * speed

	local fwd = ffi.cast("vec3 *", Cam_GetForward(camera))
	local right = ffi.cast("vec3 *", Cam_GetRight(camera))

	pos.x = pos.x + (fwd.x * velocity * I_GetAxis("forward")) + (right.x * velocity * I_GetAxis("lateral"))
	pos.y = pos.y + right.y * velocity * I_GetAxis("lateral")
	pos.z = pos.z + (fwd.z * velocity * I_GetAxis("forward")) + (right.z * velocity * I_GetAxis("lateral"))

	if I_GetButton("rot_right") == true then
		rot.y = rot.y + rotateSpeed * deltaTime
	elseif I_GetButton("rot_left") == true then
		rot.y = rot.y - rotateSpeed * deltaTime
	end

	if I_GetButton("rot_up") == true then
		rot.x = rot.x - rotateSpeed * deltaTime
	elseif I_GetButton("rot_down") == true then
		rot.x = rot.x + rotateSpeed * deltaTime
	end

	rot.x = math.min(math.max(rot.x, -60.0), 85.0)

	O_SetPosition(parent, posPtr)
	O_SetRotation(parent, rotPtr)
end

function Unload()
	I_EnableMouseAxis(false)

	return true
end
