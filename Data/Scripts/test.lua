function Load()
	lightComponent = E_NewComponent("LightComponent", parent, "type=point", "color=1.0, 0.0, 0.0", "intensity=10.0", "radius=70.0, 85.0")
	O_AddComponent(parent, "Light", lightComponent);

	return 0
end

function Update(deltaTime)
	G_DrawString("This is written from a script", 200, 200)
	G_DrawString(string.format("Version: %s | resolution: %dx%d", E_GetVersion(), E_GetScreenWidth(), E_GetScreenHeight()), 400, 400)
	G_DrawString("So is this", 200, 400)
end
